package com.example.weather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_layout.view.*

class RecyclerAdapter(private val RecyclerList: List<itemsRecycler>) : RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_layout,parent,false)
        return RecyclerViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val currentItem = RecyclerList[position]
        holder.imageView1.setImageResource(currentItem.imageResource1)
        holder.imageView2.setImageResource(currentItem.imageResource2)
        holder.textView1.text = currentItem.item1
        holder.textView2.text = currentItem.item2
        holder.textView3.text = currentItem.item3
    }

    override fun getItemCount() = RecyclerList.size

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView1: ImageView = itemView.weathericon
        val imageView2: ImageView = itemView.heart
        val textView1: TextView = itemView.tvlocate
        val textView2: TextView = itemView.tvtemp
        val textView3: TextView = itemView.tvstatus
    }
}