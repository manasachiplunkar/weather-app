package com.example.weather

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_splash.*
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class Splash : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val textView: TextView = findViewById(R.id.tvdate)
        val simpleDateFormat = SimpleDateFormat("EEE, d MMM yyyy  HH:mm a")
        val currentDateAndTime: String = simpleDateFormat.format(Date())
        textView.text = currentDateAndTime

        val toolbar = findViewById<Toolbar>(R.id.tbWeatherApp)

        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        setSupportActionBar(toolbar)
        supportActionBar?.setLogo(R.drawable.logo);
        supportActionBar?.setDisplayUseLogoEnabled(true);

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        navView.setNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.item2 -> {
                    val intent = Intent(this, favourite::class.java)
                    startActivity(intent)
                }
                R.id.item3 -> {
                    val intent = Intent(this, search::class.java)
                    startActivity(intent)
                }
            }
            true
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation(tvlocation)
        }
        else {
            ActivityCompat.requestPermissions(this as Activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 100)

        }


    }

    fun getCurrentLocation(tvlocation: TextView) {
        val fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation.addOnCompleteListener {
            val location = it.getResult()
            val geocoder: Geocoder = Geocoder(this)
            val address: List<Address> = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            tvlocation.text = "${address.get(0).subAdminArea}, ${address.get(0).adminArea}"
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                getCurrentLocation(tvlocation)
            }
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        when(item.itemId) {
            R.id.searchbarhome -> {
                val intent = PlaceAutocomplete.IntentBuilder()
                    .accessToken("pk.eyJ1IjoibWFuYXNhLWNoaXBsdW5rYXIiLCJhIjoiY2tmZHBlOXN5MXJ3eDJ5bXF5NWxkN3l6dSJ9.dz0o_YRQ3rjty_f4a4Njsg")
                    .placeOptions(PlaceOptions.builder().build(PlaceOptions.MODE_CARDS))
                    .build(this)

                startActivityForResult(intent,1)
            }
        }
        return true
        // return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            var feature = PlaceAutocomplete.getPlace(data)
            //PlaceAutocomplete.getPlace(data)
            var searchValue =  feature.text()!!
            Log.d("LogReport", "" + searchValue)
        }
    }
}