package com.example.weather

import android.app.SearchManager
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_favourite.*


class favourite : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)

        remove.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Are you sure want to remove all the favourites?")
            builder.setPositiveButton("Yes",{ dialogInterface: DialogInterface, i: Int -> })
            builder.setNegativeButton("No",{ dialogInterface: DialogInterface, i: Int -> })
            builder.show()
        }

        val recyclerList = generateDummyList(500)

        favrecycler.adapter = RecyclerAdapter(recyclerList)
        favrecycler.layoutManager = LinearLayoutManager(this)
        favrecycler.setHasFixedSize(true)

    }
    private fun generateDummyList(size: Int):List<itemsRecycler> {
        val list = ArrayList<itemsRecycler>()
        val favicon = R.drawable.icon_favourite_active
        for (i in 0 until size) {
            val drawable = when (i % 6) {
                0 -> R.drawable.icon_clear_night_small
                1 -> R.drawable.icon_mostly_cloudy_small
                2 -> R.drawable.icon_thunderstorm_small
                3 -> R.drawable.icon_partly_cloudy_small
                4 -> R.drawable.icon_rain_small
                else -> R.drawable.icon_mostly_sunny_small
            }
            val item = itemsRecycler("udupi", drawable,"3$i C", "MostlySunny", favicon)
            list += item
        }
        return list
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}

