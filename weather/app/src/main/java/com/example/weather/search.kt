package com.example.weather

import android.app.SearchManager
import android.content.Context
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_favourite.*
import kotlinx.android.synthetic.main.activity_search.*

class search : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val recyclerList = generateDummyList(500)

        searchrecycler.adapter = RecyclerAdapter(recyclerList)
        searchrecycler.layoutManager = LinearLayoutManager(this)
        searchrecycler.setHasFixedSize(true)

    }

    private fun generateDummyList(size: Int):List<itemsRecycler> {
        val list = ArrayList<itemsRecycler>()
        for (i in 0 until size) {
            val favicon = when (i % 2) {
                0 -> R.drawable.icon_favourite
                else -> R.drawable.icon_favourite_active
            }
            val drawable = when (i % 6) {
                0 -> R.drawable.icon_clear_night_small
                1 -> R.drawable.icon_mostly_cloudy_small
                2 -> R.drawable.icon_partly_cloudy_small
                3 -> R.drawable.icon_thunderstorm_small
                4 -> R.drawable.icon_rain_small
                else -> R.drawable.icon_mostly_sunny_small
            }
            val item = itemsRecycler("udupi", drawable, "3$i C", "Mostly Sunny", favicon)
            list += item
        }
        return list
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
